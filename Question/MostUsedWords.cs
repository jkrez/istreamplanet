﻿namespace Question
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    /// <summary>
    /// This class implements a method to find the most used words.
    /// </summary>
    public class MostUsedWords
    {
        /// <summary>
        /// This function returns the most used word in a block of text, where a word is defined as:
        ///     - Joined alphabetic characters.
        ///     - Hyphenated words or words split with other non alphabetic characters
        ///       will be split into two words.
        ///     - May or not exist in the English (or any other language's) dictionary.
        /// 
        /// Other implementation options:
        ///     C# linq:
        ///         words.Split(' ').Where(x => x.Length > 3)
        ///             .GroupBy(x => x)
        ///             .Select(x => new { Count = x.Count(), Word = x.Key
        ///             })
        ///             .OrderByDescending(x => x.Count).FirstOrDefault();
        ///     Unix commands:
        ///         tr -c '[:alnum:]' '[\n*]' < test.txt | sort | uniq -c | sort -nr | head  -10
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static List<string> FindMostUsedWord(string text)
        {
            if (text == null)
            {
                throw new ArgumentException(nameof(text));
            }

            // Remove all characters that aren't letters or spaces
            var lowerText = text.ToLowerInvariant();
            var regex = new Regex("[^a-z]");
            var parsedText = regex.Replace(lowerText, " ");

            // Return empty list if there's no words.
            if (String.IsNullOrWhiteSpace(text))
            {
                return new List<string>();
            }

            // Count frequency of words in hashmap.
            var wordCount = new Dictionary<string, int>();
            var maxUsedCount = 1;
            foreach (var str in parsedText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (wordCount.ContainsKey(str))
                {
                    wordCount[str] += 1;
                    if (wordCount[str] > maxUsedCount)
                    {
                        maxUsedCount = wordCount[str];
                    }
                }
                else
                {
                    wordCount[str] = 1;
                }
            }

            // Return words with the max usage.
            var MostUsedWords = new List<string>();
            foreach (var kvp in wordCount)
            {
                if (kvp.Value == maxUsedCount)
                {
                    MostUsedWords.Add(kvp.Key);
                }
            }

            return MostUsedWords;
        }
    }
}
