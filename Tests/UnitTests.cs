﻿namespace Tests
{
    using System;
    using System.Collections.Generic;
    using Question;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// This is a test class for the MostUsedWords class. This is designed to be a
    /// unit test for the class. Note that there are test cases omitted from this class
    /// such as fully exhaustive unicode character tests, performance tests, fuzzing tests,
    /// and any verification tests for components which might be integrated with the 
    /// MostUsedWords class.
    /// </summary>
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void InvalidCase()
        {
            try
            {
                MostUsedWords.FindMostUsedWord(null);
            }
            catch (Exception ex)
            {
                if (ex.GetType() != typeof(ArgumentException))
                {
                    Assert.Fail("Unexpected type of exception");
                }
            }
        }

        [TestMethod]
        public void EmptyStringTest()
        {
            Validate(new List<string>(), "");
        }

        [TestMethod]
        public void WhitespaceStringTests()
        {
            var expected = new List<string>();
            Validate(expected, "\t");
            Validate(expected, "\r");
            Validate(expected, "\n");
            Validate(expected, " \t\t\t\t\r   ");
        }

        [TestMethod]
        public void InvalidCharacterStringTests()
        {
            var expected = new List<string>();
            Validate(expected, ",.<>/?':\"`~!@#$%^&*()_+-=|\\[]{}");
        }

        [TestMethod]
        public void SimpleStringTests()
        {
            Validate(new List<string>() {"a"}, "a");
            Validate(new List<string>() {"a"}, "a a a");
            Validate(new List<string>() {"a", "b"}, "a b");
            Validate(new List<string>() {"a", "b", "c"}, "a a a b b b c c c");
            Validate(new List<string>() { "the"}, "the quick brown fox jumped over the lazy dog");
            Validate(new List<string>() {"a"}, "A");
            Validate(new List<string>() {"a","b"}, "A B");
            Validate(new List<string>() {"a","b"}, "A!B");
        }

        [TestMethod]
        public void LongStringTests()
        {
            var long1 =
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
            var long1Expected = new List<string>() {"ut", "in"};
            var long2 = "eM+CJoHTCfIr@6cQNK4ZSu_+oX9Hq+7VR c&iWk8vvCH9pTl!GZbZSDFAKIHp9*X$S3Z!iVp_oY)E-TXzc3%-sL)QY09W%09(rXo+K&N$u%o CW\r\n";
            var long2Expected = new List<string>()
            {
                "em",
                "cjohtcfir",
                "cqnk",
                "zsu",
                "ox",
                "hq",
                "vr",
                "c",
                "iwk",
                "vvch",
                "ptl",
                "gzbzsdfakihp",
                "x",
                "s",
                "z",
                "ivp",
                "oy",
                "e",
                "txzc",
                "sl",
                "qy",
                "w",
                "rxo",
                "k",
                "n",
                "u",
                "o",
                "cw"
            };

            Validate(long1Expected, long1);
            Validate(long2Expected, long2);
        }

        private void Validate(List<string> expected, string input)
        {
            var result = MostUsedWords.FindMostUsedWord(input);
            CollectionAssert.AreEqual(expected, result);
        }
    }
}
